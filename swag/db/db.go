package db

import (
	"database/sql"
	"time"

	_ "github.com/lib/pq"
)

const (
	defaultURL = "postgres://postgres:zorktest@127.0.0.1:54320/swag_dev?sslmode=disable"
)

var timeNow = time.Now

type DBOption func(*Database) error

func WithSqlDB(sqlDB *sql.DB) DBOption {
	return func(db *Database) error {
		db.sqlDB = sqlDB
		return nil
	}
}

func WithPsqlURL(psqlURL string) DBOption {
	return func(db *Database) error {
		sqlDB, err := sql.Open("postgres", psqlURL)
		if err != nil {
			return err
		}
		option := WithSqlDB(sqlDB)
		return option(db)
	}
}

//Open will create a new database with the provided psqlURL
func Open(options ...DBOption) (*Database, error) {
	db := &Database{}
	for _, option := range options {
		if err := option(db); err != nil {
			return nil, err
		}
	}
	return db, nil
}

type Campaign struct {
	ID       int
	StartsAt time.Time
	EndsAt   time.Time
	Price    int
}

type Database struct {
	sqlDB *sql.DB
}

func (db *Database) Close() error {
	return db.sqlDB.Close()
}

func (db *Database) CreateCampaign(start, end time.Time, price int) (*Campaign, error) {
	statement := `
	INSERT INTO campaigns(starts_at, ends_at, price)
	VALUES($1, $2, $3)
	RETURNING id`
	var id int
	err := db.sqlDB.QueryRow(statement, start, end, price).Scan(&id)
	if err != nil {
		return nil, err
	}
	return &Campaign{
		ID:       id,
		StartsAt: start,
		EndsAt:   end,
		Price:    price,
	}, nil
}

func (db *Database) ActiveCampaign() (*Campaign, error) {
	statement := `
	SELECT * FROM campaigns
	WHERE starts_at <= $1
	AND ends_at >= $1`
	row := db.sqlDB.QueryRow(statement, timeNow())
	var camp Campaign
	err := row.Scan(&camp.ID, &camp.StartsAt, &camp.EndsAt, &camp.Price)
	if err != nil {
		return nil, err
	}
	return &camp, nil
}

func (db *Database) GetCampaign(id int) (*Campaign, error) {
	statement := `
	SELECT * FROM campaigns
	WHERE id = $1`
	row := db.sqlDB.QueryRow(statement, id)
	var camp Campaign
	err := row.Scan(&camp.ID, &camp.StartsAt, &camp.EndsAt, &camp.Price)
	if err != nil {
		return nil, err
	}
	return &camp, nil
}

type Customer struct {
	Name  string
	Email string
}

type Address struct {
	Street1 string
	Street2 string
	City    string
	State   string
	Zip     string
	Country string
	// In case the format above fails
	Raw string
}

type Payment struct {
	Source     string
	CustomerID string
	ChargeID   string
}

type Order struct {
	ID         int
	CampaignID int
	Customer   Customer
	Address    Address
	Payment    Payment
}

func (db *Database) CreateOrder(order *Order) error {
	statement := `
	INSERT INTO orders (
		campaign_id,
		cus_name, cus_email,
		adr_street1, adr_street2, adr_city, adr_state, adr_zip, adr_country, adr_raw,
		pay_source, pay_customer_id, pay_charge_id
	)
	VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
	RETURNING id`
	err := db.sqlDB.QueryRow(statement,
		order.CampaignID,
		order.Customer.Name,
		order.Customer.Email,
		order.Address.Street1,
		order.Address.Street2,
		order.Address.City,
		order.Address.State,
		order.Address.Zip,
		order.Address.Country,
		order.Address.Raw,
		order.Payment.Source,
		order.Payment.CustomerID,
		order.Payment.ChargeID,
	).Scan(&order.ID)
	if err != nil {
		return err
	}
	return nil
}

func (db *Database) GetOrderViaPayCus(payCustomerID string) (*Order, error) {
	statement := `
	SELECT * FROM orders
	WHERE pay_customer_id = $1`
	row := db.sqlDB.QueryRow(statement, payCustomerID)
	var ord Order
	err := row.Scan(
		&ord.ID,
		&ord.CampaignID,
		&ord.Customer.Name,
		&ord.Customer.Email,
		&ord.Address.Street1,
		&ord.Address.Street2,
		&ord.Address.City,
		&ord.Address.State,
		&ord.Address.Zip,
		&ord.Address.Country,
		&ord.Address.Raw,
		&ord.Payment.Source,
		&ord.Payment.CustomerID,
		&ord.Payment.ChargeID,
	)
	if err != nil {
		return nil, err
	}
	return &ord, nil
}

func (db *Database) ConfirmOrder(orderID int, addressRaw, paymentChargeID string) error {
	statement := `UPDATE orders
	SET adr_raw = $2, pay_charge_id = $3
	WHERE id = $1`
	_, err := db.sqlDB.Exec(statement, orderID, addressRaw, paymentChargeID)
	return err
}
