package http

//Logger defines the Interface used by each handler for logging purposes.
type Logger interface {
	Printf(format string, v ...interface{})
}
