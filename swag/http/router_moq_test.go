// Code generated by moq; DO NOT EDIT.
// github.com/matryer/moq

package http

import (
	"net/http"
	"sync"
)

// Ensure, that RouterOrderHandlerMock does implement RouterOrderHandler.
// If this is not the case, regenerate this file with moq.
var _ RouterOrderHandler = &RouterOrderHandlerMock{}

// RouterOrderHandlerMock is a mock implementation of RouterOrderHandler.
//
// 	func TestSomethingThatUsesRouterOrderHandler(t *testing.T) {
//
// 		// make and configure a mocked RouterOrderHandler
// 		mockedRouterOrderHandler := &RouterOrderHandlerMock{
// 			ConfirmFunc: func(w http.ResponseWriter, r *http.Request)  {
// 				panic("mock out the Confirm method")
// 			},
// 			CreateFunc: func(w http.ResponseWriter, r *http.Request)  {
// 				panic("mock out the Create method")
// 			},
// 			NewFunc: func(w http.ResponseWriter, r *http.Request)  {
// 				panic("mock out the New method")
// 			},
// 			OrderMwFunc: func(next http.HandlerFunc) http.HandlerFunc {
// 				panic("mock out the OrderMw method")
// 			},
// 			ShowFunc: func(w http.ResponseWriter, r *http.Request)  {
// 				panic("mock out the Show method")
// 			},
// 		}
//
// 		// use mockedRouterOrderHandler in code that requires RouterOrderHandler
// 		// and then make assertions.
//
// 	}
type RouterOrderHandlerMock struct {
	// ConfirmFunc mocks the Confirm method.
	ConfirmFunc func(w http.ResponseWriter, r *http.Request)

	// CreateFunc mocks the Create method.
	CreateFunc func(w http.ResponseWriter, r *http.Request)

	// NewFunc mocks the New method.
	NewFunc func(w http.ResponseWriter, r *http.Request)

	// OrderMwFunc mocks the OrderMw method.
	OrderMwFunc func(next http.HandlerFunc) http.HandlerFunc

	// ShowFunc mocks the Show method.
	ShowFunc func(w http.ResponseWriter, r *http.Request)

	// calls tracks calls to the methods.
	calls struct {
		// Confirm holds details about calls to the Confirm method.
		Confirm []struct {
			// W is the w argument value.
			W http.ResponseWriter
			// R is the r argument value.
			R *http.Request
		}
		// Create holds details about calls to the Create method.
		Create []struct {
			// W is the w argument value.
			W http.ResponseWriter
			// R is the r argument value.
			R *http.Request
		}
		// New holds details about calls to the New method.
		New []struct {
			// W is the w argument value.
			W http.ResponseWriter
			// R is the r argument value.
			R *http.Request
		}
		// OrderMw holds details about calls to the OrderMw method.
		OrderMw []struct {
			// Next is the next argument value.
			Next http.HandlerFunc
		}
		// Show holds details about calls to the Show method.
		Show []struct {
			// W is the w argument value.
			W http.ResponseWriter
			// R is the r argument value.
			R *http.Request
		}
	}
	lockConfirm sync.RWMutex
	lockCreate  sync.RWMutex
	lockNew     sync.RWMutex
	lockOrderMw sync.RWMutex
	lockShow    sync.RWMutex
}

// Confirm calls ConfirmFunc.
func (mock *RouterOrderHandlerMock) Confirm(w http.ResponseWriter, r *http.Request) {
	if mock.ConfirmFunc == nil {
		panic("RouterOrderHandlerMock.ConfirmFunc: method is nil but RouterOrderHandler.Confirm was just called")
	}
	callInfo := struct {
		W http.ResponseWriter
		R *http.Request
	}{
		W: w,
		R: r,
	}
	mock.lockConfirm.Lock()
	mock.calls.Confirm = append(mock.calls.Confirm, callInfo)
	mock.lockConfirm.Unlock()
	mock.ConfirmFunc(w, r)
}

// ConfirmCalls gets all the calls that were made to Confirm.
// Check the length with:
//     len(mockedRouterOrderHandler.ConfirmCalls())
func (mock *RouterOrderHandlerMock) ConfirmCalls() []struct {
	W http.ResponseWriter
	R *http.Request
} {
	var calls []struct {
		W http.ResponseWriter
		R *http.Request
	}
	mock.lockConfirm.RLock()
	calls = mock.calls.Confirm
	mock.lockConfirm.RUnlock()
	return calls
}

// Create calls CreateFunc.
func (mock *RouterOrderHandlerMock) Create(w http.ResponseWriter, r *http.Request) {
	if mock.CreateFunc == nil {
		panic("RouterOrderHandlerMock.CreateFunc: method is nil but RouterOrderHandler.Create was just called")
	}
	callInfo := struct {
		W http.ResponseWriter
		R *http.Request
	}{
		W: w,
		R: r,
	}
	mock.lockCreate.Lock()
	mock.calls.Create = append(mock.calls.Create, callInfo)
	mock.lockCreate.Unlock()
	mock.CreateFunc(w, r)
}

// CreateCalls gets all the calls that were made to Create.
// Check the length with:
//     len(mockedRouterOrderHandler.CreateCalls())
func (mock *RouterOrderHandlerMock) CreateCalls() []struct {
	W http.ResponseWriter
	R *http.Request
} {
	var calls []struct {
		W http.ResponseWriter
		R *http.Request
	}
	mock.lockCreate.RLock()
	calls = mock.calls.Create
	mock.lockCreate.RUnlock()
	return calls
}

// New calls NewFunc.
func (mock *RouterOrderHandlerMock) New(w http.ResponseWriter, r *http.Request) {
	if mock.NewFunc == nil {
		panic("RouterOrderHandlerMock.NewFunc: method is nil but RouterOrderHandler.New was just called")
	}
	callInfo := struct {
		W http.ResponseWriter
		R *http.Request
	}{
		W: w,
		R: r,
	}
	mock.lockNew.Lock()
	mock.calls.New = append(mock.calls.New, callInfo)
	mock.lockNew.Unlock()
	mock.NewFunc(w, r)
}

// NewCalls gets all the calls that were made to New.
// Check the length with:
//     len(mockedRouterOrderHandler.NewCalls())
func (mock *RouterOrderHandlerMock) NewCalls() []struct {
	W http.ResponseWriter
	R *http.Request
} {
	var calls []struct {
		W http.ResponseWriter
		R *http.Request
	}
	mock.lockNew.RLock()
	calls = mock.calls.New
	mock.lockNew.RUnlock()
	return calls
}

// OrderMw calls OrderMwFunc.
func (mock *RouterOrderHandlerMock) OrderMw(next http.HandlerFunc) http.HandlerFunc {
	if mock.OrderMwFunc == nil {
		panic("RouterOrderHandlerMock.OrderMwFunc: method is nil but RouterOrderHandler.OrderMw was just called")
	}
	callInfo := struct {
		Next http.HandlerFunc
	}{
		Next: next,
	}
	mock.lockOrderMw.Lock()
	mock.calls.OrderMw = append(mock.calls.OrderMw, callInfo)
	mock.lockOrderMw.Unlock()
	return mock.OrderMwFunc(next)
}

// OrderMwCalls gets all the calls that were made to OrderMw.
// Check the length with:
//     len(mockedRouterOrderHandler.OrderMwCalls())
func (mock *RouterOrderHandlerMock) OrderMwCalls() []struct {
	Next http.HandlerFunc
} {
	var calls []struct {
		Next http.HandlerFunc
	}
	mock.lockOrderMw.RLock()
	calls = mock.calls.OrderMw
	mock.lockOrderMw.RUnlock()
	return calls
}

// Show calls ShowFunc.
func (mock *RouterOrderHandlerMock) Show(w http.ResponseWriter, r *http.Request) {
	if mock.ShowFunc == nil {
		panic("RouterOrderHandlerMock.ShowFunc: method is nil but RouterOrderHandler.Show was just called")
	}
	callInfo := struct {
		W http.ResponseWriter
		R *http.Request
	}{
		W: w,
		R: r,
	}
	mock.lockShow.Lock()
	mock.calls.Show = append(mock.calls.Show, callInfo)
	mock.lockShow.Unlock()
	mock.ShowFunc(w, r)
}

// ShowCalls gets all the calls that were made to Show.
// Check the length with:
//     len(mockedRouterOrderHandler.ShowCalls())
func (mock *RouterOrderHandlerMock) ShowCalls() []struct {
	W http.ResponseWriter
	R *http.Request
} {
	var calls []struct {
		W http.ResponseWriter
		R *http.Request
	}
	mock.lockShow.RLock()
	calls = mock.calls.Show
	mock.lockShow.RUnlock()
	return calls
}

// Ensure, that RouterCampaignHandlerMock does implement RouterCampaignHandler.
// If this is not the case, regenerate this file with moq.
var _ RouterCampaignHandler = &RouterCampaignHandlerMock{}

// RouterCampaignHandlerMock is a mock implementation of RouterCampaignHandler.
//
// 	func TestSomethingThatUsesRouterCampaignHandler(t *testing.T) {
//
// 		// make and configure a mocked RouterCampaignHandler
// 		mockedRouterCampaignHandler := &RouterCampaignHandlerMock{
// 			CampaignMwFunc: func(next http.HandlerFunc) http.HandlerFunc {
// 				panic("mock out the CampaignMw method")
// 			},
// 			ShowActiveFunc: func(w http.ResponseWriter, r *http.Request)  {
// 				panic("mock out the ShowActive method")
// 			},
// 		}
//
// 		// use mockedRouterCampaignHandler in code that requires RouterCampaignHandler
// 		// and then make assertions.
//
// 	}
type RouterCampaignHandlerMock struct {
	// CampaignMwFunc mocks the CampaignMw method.
	CampaignMwFunc func(next http.HandlerFunc) http.HandlerFunc

	// ShowActiveFunc mocks the ShowActive method.
	ShowActiveFunc func(w http.ResponseWriter, r *http.Request)

	// calls tracks calls to the methods.
	calls struct {
		// CampaignMw holds details about calls to the CampaignMw method.
		CampaignMw []struct {
			// Next is the next argument value.
			Next http.HandlerFunc
		}
		// ShowActive holds details about calls to the ShowActive method.
		ShowActive []struct {
			// W is the w argument value.
			W http.ResponseWriter
			// R is the r argument value.
			R *http.Request
		}
	}
	lockCampaignMw sync.RWMutex
	lockShowActive sync.RWMutex
}

// CampaignMw calls CampaignMwFunc.
func (mock *RouterCampaignHandlerMock) CampaignMw(next http.HandlerFunc) http.HandlerFunc {
	if mock.CampaignMwFunc == nil {
		panic("RouterCampaignHandlerMock.CampaignMwFunc: method is nil but RouterCampaignHandler.CampaignMw was just called")
	}
	callInfo := struct {
		Next http.HandlerFunc
	}{
		Next: next,
	}
	mock.lockCampaignMw.Lock()
	mock.calls.CampaignMw = append(mock.calls.CampaignMw, callInfo)
	mock.lockCampaignMw.Unlock()
	return mock.CampaignMwFunc(next)
}

// CampaignMwCalls gets all the calls that were made to CampaignMw.
// Check the length with:
//     len(mockedRouterCampaignHandler.CampaignMwCalls())
func (mock *RouterCampaignHandlerMock) CampaignMwCalls() []struct {
	Next http.HandlerFunc
} {
	var calls []struct {
		Next http.HandlerFunc
	}
	mock.lockCampaignMw.RLock()
	calls = mock.calls.CampaignMw
	mock.lockCampaignMw.RUnlock()
	return calls
}

// ShowActive calls ShowActiveFunc.
func (mock *RouterCampaignHandlerMock) ShowActive(w http.ResponseWriter, r *http.Request) {
	if mock.ShowActiveFunc == nil {
		panic("RouterCampaignHandlerMock.ShowActiveFunc: method is nil but RouterCampaignHandler.ShowActive was just called")
	}
	callInfo := struct {
		W http.ResponseWriter
		R *http.Request
	}{
		W: w,
		R: r,
	}
	mock.lockShowActive.Lock()
	mock.calls.ShowActive = append(mock.calls.ShowActive, callInfo)
	mock.lockShowActive.Unlock()
	mock.ShowActiveFunc(w, r)
}

// ShowActiveCalls gets all the calls that were made to ShowActive.
// Check the length with:
//     len(mockedRouterCampaignHandler.ShowActiveCalls())
func (mock *RouterCampaignHandlerMock) ShowActiveCalls() []struct {
	W http.ResponseWriter
	R *http.Request
} {
	var calls []struct {
		W http.ResponseWriter
		R *http.Request
	}
	mock.lockShowActive.RLock()
	calls = mock.calls.ShowActive
	mock.lockShowActive.RUnlock()
	return calls
}
