package stripe_test

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"twg/stripe"
)

var (
	apiKey string
	update bool
)

const (
	tokenAmex               = "tok_amex"
	tokenVisaDebit          = "tok_visa_debit"
	tokenMastercardPrepaid  = "tok_mastercard_prepaid"
	tokenInvalid            = "tok_gnilpth"
	tokenExpiredCard        = "tok_chargeDeclinedExpiredCard"
	tokenIncorrectCVC       = "tok_chargeDeclinedIncorrectCVC"
	tokenInsufficientFunds  = "tok_chargeDeclinedInsufficientFunds"
	tokenChargeCustomerFail = "tok_chargeCustomerFail"
)

func init() {
	flag.StringVar(&apiKey, "key", "", "Your TEST secret key for the Stripe API. If present, integration tests will be run using this key.")
	flag.BoolVar(&update, "update", false, "Set this flag to update the responses used in local tests. This requires interaction with the Stripe API, so the key flag must be set.")
}

func TestClient_Local(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprint(w, `{
			"id": "cus_4QEipX9Dj5Om1P",
			"object": "customer",
			"address": null,
			"balance": 0,
			"created": 1405639479,
			"currency": "usd",
			"default_source": "card_1Iv3wZ2eZvKYlo2CcwHFSlD4",
			"delinquent": false,
			"description": "My First Test Customer (created for API docs)",
			"discount": null,
			"email": "test@test.com",
			"invoice_prefix": "4090BFB",
			"invoice_settings": {
			  "custom_fields": null,
			  "default_payment_method": null,
			  "footer": null
			},
			"livemode": false,
			"metadata": {
			  "what is metadata": "set of key value pairs that you can attach to  an object"
			},
			"name": null,
			"next_invoice_sequence": 83161,
			"phone": null,
			"preferred_locales": [],
			"shipping": null,
			"tax_exempt": "none"
		  }`)
	})
	server := httptest.NewServer(mux)
	defer server.Close()
	c := stripe.Client{
		Key:     "bullshit_key",
		BaseURL: server.URL,
	}
	_, err := c.Customer("bullshit token", "bullshit email")
	if err != nil {
		t.Fatalf("err = %v; want nil", err)
	}
}

func stripeClient(t *testing.T) (*stripe.Client, func()) {
	teardown := make([]func(), 0)
	c := stripe.Client{
		Key: apiKey,
	}
	if apiKey == "" {
		count := 0
		handler := func(w http.ResponseWriter, r *http.Request) {
			resp := readResponse(t, count)
			w.WriteHeader(resp.StatusCode)
			w.Write(resp.Body)
			count++
		}
		server := httptest.NewServer(http.HandlerFunc(handler))
		c.BaseURL = server.URL
		teardown = append(teardown, server.Close)
	}
	if update {
		rc := &recorderClient{}
		c.HttpClient = rc
		teardown = append(teardown, func() {
			for i, res := range rc.responses {
				recordResponse(t, res, i)
			}
		})
	}
	return &c, func() {
		for _, fn := range teardown {
			fn()
		}
	}
}

func responsePath(t *testing.T, count int) string {
	return filepath.Join("testdata", filepath.FromSlash(fmt.Sprintf("%s.%d.json", t.Name(), count)))
}

func readResponse(t *testing.T, count int) response {
	var resp response
	path := responsePath(t, count)
	f, err := os.Open(path)
	if err != nil {
		t.Fatalf("failed to open the response file: %s. err = %v", path, err)
	}
	defer f.Close()
	jsonBytes, err := ioutil.ReadAll(f)
	if err != nil {
		t.Fatalf("failed to read the response file: %s. err = %v", path, err)
	}
	err = json.Unmarshal(jsonBytes, &resp)
	if err != nil {
		t.Fatalf("failed to json unmarshal the response file: %s. err = %v", path, err)
	}
	return resp
}

func recordResponse(t *testing.T, resp response, count int) {
	path := responsePath(t, count)
	err := os.MkdirAll(filepath.Dir(path), 0700)
	if err != nil {
		t.Fatalf("failed to create the response dir: %s. err = %v", filepath.Dir(path), err)
	}
	f, err := os.Create(path)
	if err != nil {
		t.Fatalf("failed to create the response file: %s. err = %v", path, err)
	}
	jsonBytes, err := json.MarshalIndent(resp, "", "  ")
	if err != nil {
		t.Fatalf("failed marshal JSON for response file: %s. err = %v", path, err)
	}
	_, err = f.Write(jsonBytes)
	if err != nil {
		t.Fatalf("failed to write json bytes for response file: %s. err = %v", path, err)
	}
}

func TestClient_Customer(t *testing.T) {
	if apiKey == "" {
		t.Log("No API key provided, runing unit tests using recorded responses. Be sure to run the tests against a real API before committing.")
	}

	type checkFn func(*testing.T, *stripe.Customer, error)
	check := func(fns ...checkFn) []checkFn { return fns }

	hasNoErr := func() checkFn {
		return func(t *testing.T, cus *stripe.Customer, err error) {
			if err != nil {
				t.Errorf("Customer() err = %v; want %v", err, nil)
			}
		}
	}

	hasErrType := func(etype string) checkFn {
		return func(t *testing.T, cus *stripe.Customer, err error) {
			se, ok := err.(stripe.Error)
			if !ok {
				t.Fatalf("error is not a stripe.Error")
			}
			if se.Type != etype {
				t.Errorf("err.Type = %s; want %s", se.Type, etype)
			}
		}
	}
	hasIDPrefix := func() checkFn {
		return func(t *testing.T, cus *stripe.Customer, err error) {
			if !strings.HasPrefix(cus.ID, "cus_") {
				t.Errorf("ID = %s; want prefix %q", cus.ID, "cus_")
			}
		}
	}
	hasCardDefaultSource := func() checkFn {
		return func(t *testing.T, cus *stripe.Customer, err error) {
			if !strings.HasPrefix(cus.DefaultSource, "card_") {
				t.Errorf("DefaultSource = %s; want prefix %q", cus.DefaultSource, "card_")
			}
		}
	}
	hasEmail := func(email string) checkFn {
		return func(t *testing.T, cus *stripe.Customer, err error) {
			if cus.Email != email {
				t.Errorf("Email = %s; want %s", cus.Email, email)
			}
		}
	}

	tests := map[string]struct {
		token  string
		email  string
		checks []checkFn
	}{
		"valid customer with amex": {
			token:  tokenAmex,
			email:  "carmilla@blut.org",
			checks: check(hasNoErr(), hasIDPrefix(), hasCardDefaultSource(), hasEmail("carmilla@blut.org")),
		},
		"invalid token": {
			token:  tokenInvalid,
			email:  "carmilla@blut.org",
			checks: check(hasErrType(stripe.ErrTypeInvalidRequest)),
		},
		"expired card": {
			token:  tokenExpiredCard,
			email:  "carmilla@blut.org",
			checks: check(hasErrType(stripe.ErrTypeCardError)),
		},
		/*"incorrect cvc": {
			token:  tokenIncorrectCVC,
			email:  "carmilla@blut.org",
			checks: check(hasErrType(stripe.ErrTypeCardError)),
		},*/
		"insufficient funds": {
			token:  tokenInsufficientFunds,
			email:  "carmilla@blut.org",
			checks: check(hasErrType(stripe.ErrTypeCardError)),
		},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			c, teardown := stripeClient(t)
			defer teardown()
			cus, err := c.Customer(tc.token, tc.email)
			for _, check := range tc.checks {
				check(t, cus, err)
			}
		})
	}
}

func TestClient_Charge(t *testing.T) {
	if apiKey == "" {
		t.Log("No API key provided, runing unit tests using recorded responses. Be sure to run the tests against a real API before committing.")
	}

	type checkFn func(*testing.T, *stripe.Charge, error)
	check := func(fns ...checkFn) []checkFn { return fns }

	hasNoErr := func() checkFn {
		return func(t *testing.T, charge *stripe.Charge, err error) {
			if err != nil {
				t.Errorf("Charge() err = %v; want %v", err, nil)
			}
		}
	}

	hasAmount := func(amount int) checkFn {
		return func(t *testing.T, charge *stripe.Charge, err error) {
			if charge.Amount != amount {
				t.Errorf("Amount = %d; want %d", charge.Amount, amount)
			}
		}
	}

	hasErrType := func(etype string) checkFn {
		return func(t *testing.T, charge *stripe.Charge, err error) {
			se, ok := err.(stripe.Error)
			if !ok {
				t.Fatalf("error is not a stripe.Error")
			}
			if se.Type != etype {
				t.Errorf("err.Type = %s; want %s", se.Type, etype)
			}
		}
	}

	customerViaToken := func(token string) func(*testing.T, *stripe.Client) string {
		return func(t *testing.T, c *stripe.Client) string {
			email := "carmilla@blut.org"
			cus, err := c.Customer(token, email)
			if err != nil {
				t.Errorf("err creating customer with token %s. err = %v, want nil", token, err)
			}
			return cus.ID
		}
	}

	tests := map[string]struct {
		customerID func(*testing.T, *stripe.Client) string
		amount     int
		checks     []checkFn
	}{
		"valid charge w amex": {
			customerID: customerViaToken(tokenAmex),
			amount:     1234,
			checks:     check(hasNoErr(), hasAmount(1234)),
		},
		"valid charge w MastercardPrepaid": {
			customerID: customerViaToken(tokenMastercardPrepaid),
			amount:     666,
			checks:     check(hasNoErr(), hasAmount(666)),
		},
		"valid charge w VisaDebit": {
			customerID: customerViaToken(tokenVisaDebit),
			amount:     4321,
			checks:     check(hasNoErr(), hasAmount(4321)),
		},
		"invalid customer id": {
			customerID: func(*testing.T, *stripe.Client) string {
				return "cus.none"
			},
			amount: 1234,
			checks: check(hasErrType(stripe.ErrTypeInvalidRequest)),
		},
		"charge failure": {
			customerID: customerViaToken(tokenChargeCustomerFail),
			amount:     1010,
			checks:     check(hasErrType(stripe.ErrTypeCardError)),
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			c, teardown := stripeClient(t)
			defer teardown()
			cusID := tc.customerID(t, c)
			charge, err := c.Charge(cusID, tc.amount)
			for _, check := range tc.checks {
				check(t, charge, err)
			}

		})
	}

}
